<?php
$pageTitle = 'Programas TurClub';
$pageDescription = 'Beneficio que te ofrece Turbus si viajas frecuentemente por motivos de estudios';
 
include('includes/head.php');
?>
<body class="tb-page">
	<?php include 'includes/menu.php' ?>

	<img class="img-responsive tb-banner__img" src="img/banner-beneficios-3.jpg" alt="">
	<section class="tb-page--puntos container">
		<!-- <h2>Programas para Estudiantes y Trabajadores</h2> -->
		<div class="row">
			<div class="col-sm-6 ">
				<div class="tb-page--programas__content tb-bg-grey">
					<div class="tb-page--programas__section">
						<h3 class="tb-page--programas__big-title">Estudiantes</h3>
						<p class="">Este programa es un beneficio que te ofrece Turbus si viajas frecuentemente por motivos de estudios, accediendo a una tarifa rebajada en un tramo solicitado (ida y vuelta), previa inscripción.</p>
					
					</div>

					<div class="tb-page--programas__section">
						<h4>¿Cómo inscribirme?</h4>
						<p>Dirígete a tu punto de venta Turbus más cercano, presentando la siguiente documentación:</p>
						<ul class="tb-pasenger-list">
							<li>Fotocopia de tu cédula de identidad.</li>
							<li>Certificado de alumno regular de tu casa de estudios o comprobante de matrícula.</li>
						</ul>
						<p>Además debes indicar por escrito (en la fotocopia de tu cédula de identidad) lo siguiente:</p>
						<ul class="tb-pasenger-list">
							<li>Tu email </li>
							<li>Los tramos solicitados </li>
							<li>Celular</li>
						</ul>
						<h5>Activa tu contraseña web para comprar online y acceder a los beneficios Turclub</h5>
						<p>Inscríbete como socio Turclub por la web, para que generes tu contraseña.</p>
					</div>

					<div class="tb-page--programas__section">
						<h4>¿Cómo hago efectiva la tarifa rebajada?</h4>
						<ul class="tb-pasenger-list">
							<li><strong>De manera presencial:</strong> En cualquiera de nuestras oficinas de venta, presentando tu cédula de identidad o TNE.</li>
							<li><strong>Por nuestra página web:</strong> Ingresando a www.turbus.cl , y dirígete a Acceso Socios. Ingresas tu rut y contraseña y ya podrás comprar tu pasaje.</li>
						</ul>
						<small>Puedes seleccionar la opción de imprimir tu pasaje en tu domicilio para que te embarques directo al bus; o bien optar por imprimir el boleto en el pasajemático o cualquier ventanilla de nuestras oficinas.</small>
					</div>

					<div class="tb-page--programas__section">
						<h4>Restricciones</h4>
						<ul class="tb-pasenger-list">
							<li>Descuento válido “SOLO” para el tramo indicado en los documentos que entregaste en nuestras oficinas. </li>
							<li>Tope de 1 pasaje diario (ida y vuelta) para el tramo elegido. </li>
							<li>Vigencia hasta el 20/12/2018, sujetos a disponibilidad y cupos limitados.</small></li>
						</ul>
					</div>

					<div class="tb-page--programas__section">
						<h4>Restricciones</h4>
						<ul class="tb-pasenger-list">
							<li>El mal uso del beneficio será causal de eliminación del programa.</li>
							<li>Se validará que quien viaja es efectivamente el beneficiario.</li>
							<li>Todo estudiante que ingrese al Programa Estudiante de Turbus quedará automáticamente habilitado en el programa de beneficio de Cóndor, accediendo a solo 1 pasaje diario (ida y vuelta) en cualquier bus (Turbus o Cóndor).</li>
							<li>Los cambios de boleto se regirán por la tarifa del momento, si existe alguna diferencia, deberá ser cancelada en el momento por el usuario para hacer efectivo el cambio.</li>
							<li>Tarifas exclusivas para estudiantes con boletos confirmados (hora, fecha y asiento).</li>
							<li>El programa estudiante de Turbus no acumula puntos en el tramo elegido para el descuento.</li>
						</ul>
					</div>

					<div class="tb-page--programas__section">
						<h4>Término del programa por parte de la Empresa</h4>
						<p>La Empresa Turbus pondrá término al beneficio en cualquier momento, con o sin excepción de la causa, bastando para ello dar aviso a sus socios de la situación, a través de los medios que se estime pertinente y/o a través de los medios generales de comunicación que utiliza.</p>
					</div>
				
				</div>


			</div>

			<div class="col-sm-6">

				<div class="tb-page--programas__content tb-bg-grey">

					<div class="tb-page--programas__section">
						<h3 class="tb-page--programas__big-title">Trabajadores</h3>
						<p class="">Tenemos el beneficio para aquellos trabajadores que viajan frecuentemente en Turbus entre Santiago y la Quinta región.</p>
						<p>Accederás a una Tafira fija en un tramo definido al momento de la inscripción. </p>
					</div>

					<div class="tb-page--programas__section">
						<h4>¿Cómo inscribirme?</h4>
						<p>Tienes que dirigirte a nuestros puntos de venta, presentado la siguiente documentación:</p>
						<ul class="tb-pasenger-list">
							<li>Certificado de residencia o cuenta a su nombre (cuenta de servicio básico, cuenta de casa comercial, etc) para acreditar la ciudad de dormitorio.</li>
							<li>Certificado laboral para acreditar la ciudad laboral.</li>
							<li>Fotocopia de cédula de identidad. </li>
						</ul>
					</div>

					<div class="tb-page--programas__section">
						<h4>¿Cómo compro los pasajes?</h4>
						<ul class="tb-pasenger-list">
							<li><strong>De manera presencial:</strong> En cualquiera de nuestras oficinas de venta, presentando tu cédula de identidad.</li>
							<li><strong>Por nuestra página web:</strong> Inscríbete como socio Turclub en nuestra página web, para que generes tu contraseña. Al ingresar con tus datos y contraseña, ya puedes comprar tu pasaje.</li>
						</ul>
						<small>Puedes seleccionar la opción de imprimir tu pasaje en tu domicilio para que te embarques directo al bus; o bien optar por imprimir el boleto en el pasajemático o cualquier ventanilla de nuestras oficinas.</small>
					</div>

					<div class="tb-page--programas__section">
						<h4>Restricciones</h4>
						<ul class="tb-pasenger-list">
							<li>Descuento válido solo para el tramo indicado al momento de la inscripción. </li>
							<li>Tope de 1 pasaje diario (ida y vuelta) para el tramo solicitado.</li>
							<li>Los viajes puedes realizarse de Lunes a Viernes, exceptuando festivos.</li>
							<li>Vigencia hasta el 31/12/2018, sujetos a disponibilidad y con cupos limitados.</li>
							<li>Boletos son nominativos e intransferibles.</li>
						</ul>
					</div>

					<div class="tb-page--programas__section">
						<h4>Consideraciones</h4>
						<ul class="tb-pasenger-list">
							<li>Cualquier mal uso del beneficio será causal de eliminación del programa.</li>
							<li>Se validará que quien viaja es efectivamente el beneficiario. Al momento de la compra y embarque se solicitará identificación del pasajero. </li>
							<li>Las tarifas son fijas para los siguientes grupos: </li>
						</ul>
					</div>
					<div style="background:white; display:table; width:100%">
						<div class="tb-tables cb">
							<div class="col-md-6">
								<table class="table table-striped">
										<thead>
											<td>
												<h4 class="tb-table--subtitle2"><strong>Grupo 1</strong></h4>
											</td>
										</thead>
										<tbody>
											<tr>
												<td><strong>Tramo</strong></td>
												<td><strong>Precio</strong></td>
											</tr>
											<tr>
												<td>Viña del Mar – Santiago</td>
												<td>$2.550</td>
											</tr>
											<tr>
												<td>Valparaíso – Santiago</td>
												<td>$2.550</td>
											</tr>
											<tr>
												<td>Villa Alemana – Santiago</td>
												<td>$2.550</td>
											</tr>
											<tr>
												<td>Quilpué - Santiago</td>
												<td>$2.550</td>
											</tr>
											<tr>
												<td>Curauma – Santiago</td>
												<td>$2.550</td>
											</tr>
										</tbody>
									</table>
							</div>

							<div class="col-md-6">
								<table class="table table-striped">
										<thead>
											<td>
												<h4 class="tb-table--subtitle2"><strong>Grupo 2</strong></h4>
											</td>
										</thead>
										<tbody>
											<tr>
												<td><strong>Tramo</strong></td>
												<td><strong>Precio</strong></td>
											</tr>
											<tr>
												<td>Algarrobo – Santiago</td>
												<td>$2.300</td>
											</tr>
											<tr>
												<td>El Quisco – Santiago</td>
												<td>$2.300</td>
											</tr>
											<tr>
												<td>El Tabo - Santiago</td>
												<td>$2.300</td>
											</tr>
										</tbody>
									</table>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="tb-page--programas__section">
						<ul class="tb-pasenger-list">
							<li>Los cambios de boletos se regirán por la tarifa del momento, si existe alguna diferencia deberá ser cancelada en el momento por el usuario para hacer efectivo el cambio y se regirán por la normativa vigente.</li>
							<li>Tarifas exclusivas para pasajeros que están dentro del programa Trabajador con boletos confirmados (hora, fecha y asiento).</li>
							<li>El pasajero deberá realizar como mínimo 24 viajes mensuales, lo que equivalen a 3 idas y vueltas semanales, y se evaluará trimestralmente su comportamiento para determinar su continuidad en el programa Trabajador.</li>
							<li>Este programa no acumula Puntos Turclub en el tramo elegido para el descuento. </li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</section>

<?php include 'includes/footer.php' ?>


<?php include 'includes/scripts.php'; ?>

</body>
</html>