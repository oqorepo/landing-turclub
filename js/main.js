$(document).ready(function () {

	$(function () {
		$("#tb-primary-menu a").each(function () {
			if (this.href == window.location) {
				$(this).addClass("tb-menu--selected");
			};
		});
	});

	$(window).on('load', function () {
		$('.cd-loader').fadeOut('slow', function () {
			$(this).remove();
		});
	});


	function setPercentage(value) {
		//let percentage = document.getElementById('input').value;
		document.documentElement.style.setProperty('--bar-percentage', value);

	}

	$('.block .porcent-click').on('click', function () {
		var there = $(this);
		setPercentage(there.attr('data-value'));
		$('.block .content').removeClass('active')
		$(this).parent().find('.content').addClass('active')
	});


	$('.block').on('click', function () {
		$('.block .response').removeClass('active')
		$(this).find('.response').addClass('active')
	});

	$('.block.uno').on('click', function () {
		$('.bg-image').removeClass('active')
		$('.bg-uno').addClass('active')
	});
	$('.block.dos').on('click', function () {
		$('.bg-image').removeClass('active')
		$('.bg-dos').addClass('active')
	});
	$('.block.tres').on('click', function () {
		$('.bg-image').removeClass('active')
		$('.bg-tres').addClass('active')
	});
	$('.block.cuatro').on('click', function () {
		$('.bg-image').removeClass('active')
		$('.bg-cuatro').addClass('active')
	});
	$('.block.cinco').on('click', function () {
		$('.bg-image').removeClass('active')
		$('.bg-cinco').addClass('active')
	});

	$('.menu-click').on('click', function (event) {
		event.preventDefault();
		$('header').addClass('active')
	});

	$('.close').on('click', function (event) {
		event.preventDefault();
		$('header').removeClass('active')
	});

	$("[data-toggle=popover]").popover();

	$('[data-toggle=popover]').on('click', function (e) {
		$('[data-toggle=popover]').not(this).popover('hide');
	});

	$('body').on('click', function (e) {
		//did not click a popover toggle or popover
		if ($(e.target).data('toggle') !== 'popover'
			&& $(e.target).parents('.popover.in').length === 0) { 
			$('[data-toggle="popover"]').popover('hide');
		}
	});

});