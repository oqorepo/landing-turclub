<footer id="footer" class="container-fluid">
    <div class="container">
        <p>
            <span>Empresa de Transportes Rurales Ltda.</span>
            <span class="border">Todos los derechos reservados</span>
            <a href="http://www.turbus.cl" target="_blank" class="logo">
                <img src="img/logo-footer.png" alt="Turbus">
            </a>
        </p>
    </div>
</footer>