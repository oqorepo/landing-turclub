<nav class="tb-nav--top">
	<div class="wrap">
		<a class="btn menu-click" href="#">MEN&Uacute;</a>
		<div class="group-btn">
			<a class="btn" href="https://www.turbus.cl/wtbus/pages/RegistroDeClientes/registroDeClientes.jsf" target="_blank">&Uacute;NETE AQU&Iacute;</a>
			<a class="btn" href="" data-toggle="modal" data-target="#accesoModal">ACCESO SOCIOS</a>
		</div>
	</div>
</nav>

<header class="tb-nav">
	<div class="close"><a href="#"><p>x</p></a></div>
	<nav class="tb-primary-nav">
		<p class="title">MEN&Uacute;</p>
		<ul id="tb-primary-menu">
			<li><a class="" href="index.php">HOME</a></li>
			<li><a href="beneficios.php">BENEFICIOS</a></li>
			<li><a href="puntos-turclub.php">Puntos Turclub</a></li>
			<li><a href="programas.php">PROGRAMAS</a></li>
		</ul>
		<div class="contacto">
			<img src="" alt="">
			<p>CONTACTO</p>
			<a href="tel:600 660 6600">600 660 6600</a>
		</div>
		<br>
		<div class="contacto">
			<a href="https://www.facebook.com/TurBus/"><img src="img/face.png" style="width:30px; height:30px;"></a>
			<a href="https://www.instagram.com/turbuscl/"><img src="img/instagram.png" style="width:30px; height:30px;"></a>
			<a href="https://twitter.com/turbuschile"><img src="img/twitter.png" style="width:30px; height:30px;"></a>
		</div>
	</nav class="tb-primary-nav">
</header>
<!--Precargador-->
<div class="cd-loader">
	<div class="cd-loader__grid">
		<div class="cd-loader__item"></div>
		<br>
		<p class="text-center loader-text">Cargando...</p>
	</div>
</div>