<!-- Modal Unete-->
<div class="modal fade" id="uneteModal" tabindex="-1" role="dialog" aria-labelledby="uneteModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" ><img src="img/close-button.png" alt=""></button>
        <h4 class="modal-title" id="uneteModalLabel">Unete a TurClub</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Acceso -->
<div class="modal fade" id="accesoModal" tabindex="-1" role="dialog" aria-labelledby="accesoModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content tb-acceso-modal">
      <div class="text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="img/close-button.png" aria-hidden="true" alt=""></button>
        <img src="img/logo-clean.png" alt="">
        <h4 class="modal-title" id="accesoModalLabel">INICIAR SESIÓN</h4>
        <P>Utiliza los datos de tu cuenta TurClub</P>
      </div>
      <div class="modal-body">
        <form class="tb-acceso-modal__form">
            <div class="form-group">
                <!-- <label for="exampleInputEmail1">Email address</label> -->
                <input type="text" class="form-control" id="inputRut" placeholder="RUT">
            </div>
            <div class="form-group">
                <!-- <label for="exampleInputPassword1">Password</label> -->
                <input type="email" class="form-control" id="inputEmail" placeholder="Email">
            </div>
            <div class="checkbox">
                <label>
                <input class="fl" type="checkbox"> Recordar
                </label>
                <a href="#" class="fr" style="color:white;">¿Olvidó su contraseña?</a>
            </div>
            <button type="submit" class="tb-modal--btn">INGRESAR</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="js/main.js"></script>