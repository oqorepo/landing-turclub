<?php
// Define variables for SEO
$pageTitle = 'Beneficios TurClub';
$pageDescription = 'Beneficios exclusivos para nuestros socios';
 
// Include header file
include('includes/head.php');

?>
<body class="tb-page">
	<?php include 'includes/menu.php' ?>
	<img class="img-responsive tb-banner__img" src="img/banner-beneficios.jpg" alt="">
	<section class="tb-beneficios container">
		<div class="text-center">
			<h2 class="tb-beneficios--title" >Beneficio exclusivo para clientes inscritos en Turclub</h2>
			<hr>
		</div>
		
		<div class="row row-eq-height">
			<!-- SAXOLINE -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-01.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--percent ">15%</span>
								<small>dcto.</small>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">Al comprar bolsos de viajes en <a href="https://www.saxoline.cl/">www.saxoline.cl</a></p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Para obtener este beneficio, elige el producto que quieres e ingresa el código de descuento “TURCLUB” en el carrito de compras antes de pagar.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>

			<!-- WENDYS -->
			<article class=" col-sm-4">
				<div class="tb-article--cart">
				
					<img src="img/promo-02.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--percent">20%</span>
								<small>dcto.</small>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">Por la compra de tu Combo Bacon Deluxe Single. Más información <a href="https://www.saxoline.cl/">www.wendys.cl</a></p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Puedes obtener este beneficio, presentando tu tarjeta virtual Turclub junto a tu cédula de identidad en el punto de venta.">Condiciones</a></small>
                        </p>
					</div>
				</div>

			</article>

			<!-- GANJA EDUCATIVA -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-03.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--percent">2X1</span>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">En la entrada de niños en Granja Educativa de Lónquen</p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Puedes obtener este beneficio, presentando tu tarjeta virtual Turclub junto a tu cédula de identidad en el punto de venta.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>

		</div>

		<div class="row row-eq-height">
			<!-- JUAN MAESTRO -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-04.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--text">2 promos en dcto.</span>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text"><strong>Promo 1:</strong> Mini Batido Gratis por la compra de cualquier combo.<br><br><strong>Promo 2:</strong> Disfruta 2 Sándwich Palta mayo por solo $2.990</p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Puedes obtener este beneficio, presentando tu tarjeta virtual Turclub junto a tu cédula de identidad en el punto de venta.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>

			<!-- HOTEL PATAGONIA -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-05.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
							<span class="tb-article--cart__descount--percent-text">30% <br><span class="tb-descount-day">y</span><br><br> 20% <br><span class="tb-descount-day">de dcto.</span></span>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">Disfruta un 30% de dcto. de domingo a jueves Y 20% de dcto. los días viernes y sábado. </p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Puedes obtener este beneficio, presentando tu tarjeta virtual Turclub junto a tu cédula de identidad en el punto de venta.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>

			<!-- COÑAPIRE -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-06.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
							<span class="tb-article--cart__descount--percent-text">30% <br><span class="tb-descount-day">y</span><br><br> 20% <br><span class="tb-descount-day">de dcto.</span></span>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">Disfruta un 30% de dcto. de domingo a jueves Y 20% de dcto. los días viernes y sábado. </p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Puedes obtener este beneficio, presentando tu tarjeta virtual Turclub junto a tu cédula de identidad en el punto de venta.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>

		</div>

		<div class="row row-eq-height">
			<!-- KODAK -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-07.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--text">Pack fotos</span>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">50 fotos 10x15 a $4.450 <br>50 fotos 13x18 a $4.950 <br>100 fotos 10x15 a $8.900 <br>100 fotos 13x18 a $9.900 <br>	Además + $990 lleva una ampliación de 20x30</p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 30 de Junio 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Puedes obtener este beneficio, presentando tu tarjeta virtual Turclub junto a tu cédula de identidad en el punto de venta. No acumulable con otras promociones y/o descuentos. Estas promociones son para fotos diferidas, entrega sujeta a disponibilidad de cada tienda. No válida para fotos al instante e impresas en kioscos.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>

			<!-- GOTTA -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-08.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--percent">20%</span>
								<small>dcto.</small>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">Comprando tu calzado en <a href="https://gotta.cl/">www.gotta.cl</a></p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Para obtener este beneficio, elige el producto que quieres e ingresa ingresa el código “GOTTA-TURCLUB” en la sección “cupón de descuento” del carrito de compras.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>

			<!-- HAND & CO-->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-09.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--text">Dcto. Packs de belleza. </span>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">Super Fast 2: Manicure y pedicure + exfoliación + triple parafinoterapia + esmalte a elección a solo $24.500 <br> <small>(precio referencial $30.100)</small></p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="El descuento es válido sólo para compras online.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>
		</div>
		<div class="row row-eq-height">

			<!-- PILLIN -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-10.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--percent">20%</span>
								<small>dcto.</small>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">En el total de tu compra en todas las tiendas Pillin del país. </p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
                            <small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Puedes obtener este beneficio, presentando tu tarjeta virtual Turclub junto a tu cédula de identidad en el punto de venta.">Condiciones</a></small>
                        </p>
					</div>
				</div>
			</article>

			<!-- TWEEN WAY -->
			<article class="col-sm-4">
				<div class="tb-article--cart">
					<img src="img/promo-11.jpg" alt="Beneficio Turclub" class="img-responsive tb-article--cart__img">
					<hr>
					<div class="tb-article--cart__content row-eq-height">
						<div class="col-md-3 tb-article--cart__promo">
							<p class="tb-article--cart__descount">
								<span class="tb-article--cart__descount--percent">20%</span>
								<small>dcto.</small>
							</p>
						</div>
					
						<div class="col-md-9 tb-article--cart__content">
							<p class="tb-article-cart__content-text">En el total de tu compra en todas las tiendas Tween Way del país. </p>
						</div>
					</div>
					<div class="text-center tb-article--cart__validate">
						<p class="row">
							<small class="col-xs-8">Válido hasta el 31 de Agosto 2018</small><small class="col-xs-4"><a type="button" class="tb-beneficios--toltip__link" data-container="body" data-toggle="popover" data-placement="top" data-content="Puedes obtener este beneficio, presentando tu tarjeta virtual Turclub junto a tu cédula de identidad en el punto de venta.">Condiciones</a></small>
						</p>
					</div>
				</div>
			</article>

			<div class="tb-article--cart col-sm-4 " style="border:none!important;"></div>

		</div>
	</section>
	<section class="tb-beneficios__condiciones container">
        <div class="row">
            <h2>Condiciones Generales</h2>
            <hr>
            <p>Beneficio exclusivo para clientes inscritos en Turclub. Las ofertas, descuentos, entregas de beneficio, servicio y calidad del producto será de exclusiva responsabilidad del comercio, sin responsabilidad ni injerencia alguna para Turbus. El descuento debe solicitarse en el punto de compra. No acumulable con otras promociones y/o descuentos.</p>
        </div>
	</section>

	<?php include 'includes/footer.php' ?>

	<?php include 'includes/scripts.php'; ?>

</body>


</html>