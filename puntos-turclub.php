<?php
$pageTitle = 'Puntos + Dinero = Tu Pasaje';
$pageDescription = 'Para canjear ya no necesitarás contar con una cantidad de puntos igual al 100% del valor de un pasaje.';
 
include('includes/head.php');
?>

<body class="tb-page">

	<?php include 'includes/menu.php'; ?>

	<img class="img-responsive tb-banner__img" src="img/banner-beneficios-2.jpg" alt="">
	<section class="tb-page--puntos container">
		<div class="row tb-page--puntos__graph">
			<div class="col-sm-6">
				<div class="text-center">
					<img class="tb-page--puntos__img" src="img/puntos.png" alt="">
				</div>
				<div class="tb-page--puntos__img--content">
					<p class="tb-page--puntos__img--text text-center"><span class="text-big"> Tus puntos se convierten en dinero</span> En el programa de beneficios Turclub podrás canjear tus puntos en la compra de tu pasaje.</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="text-center">
					<img class="tb-page--puntos__img" src="img/puntos2.png" alt="">
				</div>
				<div class="tb-page--puntos__img--content">
					<p class="tb-page--puntos__img--text text-center"><span class="text-big">Si tu puntos no alcanzan, súmale dinero.</span> Si no cuentas con una cantidad de puntos igual al valor de tu pasaje, ahora puedes sumarle dinero para completar el valor del pasaje.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="container-fluid bg-grey">

		<div class="container">
			<div class="row">
				<h2>Categoría de Pasajeros</h2>
				<br>
				<div class="col-lg-6">
					<p>Al ser parte de este gran club de beneficios, ingresas a una categoría según el rango de compra de pasajes que realices en Turbus:</p>
				</div>
				<ul class="col-lg-6 tb-pasenger-list">
					<li><strong class="tb-big-text">Pasajero Turista:</strong> Pasajero que presenta compras menores a $50.000 en menos de 6 meses.</li>
					<li><strong class="tb-big-text">Pasajero Frecuente:</strong> Pasajero que presenta compras superiores a $50.000 y menos a $150.000 durante 6 meses.</li>
					<li><strong class="tb-big-text">Pasajero Plata:</strong> Pasajero que presenta compras superiores a $150.000 y menores a $300.000 durante 6 meses.</li>
					<li><strong class="tb-big-text">Pasajero Oro:</strong> Pasajero que presenta compras superiores a $300.000 durante el periodo de 6 meses.</li>
				</ul>
			</div>
		</div>
	</section>
	<section class="container">
		<div class="row">
			<h2>Acumulación de Puntos Turclub</h2>
			<hr class="black-hr">
			<div class="col-lg-6">
				<p>Desde el 1 de Junio del 2018, la acumulación de puntos según categoría se ha modificado al siguiente detalle:</p>
				<a href="https://www.turbus.cl/wtbus/pages/RegistroDeClientes/registroDeClientes.jsf">
					<img class="img-responsive" src="img/inv-tb.jpg" alt="">
				</a>
			</div>
			<div class="col-lg-6">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<td>
								<h4 class="tb-table--subtitle"><strong>Categoría de Pasajeros</strong></h4>
							</td>
							<td>
								<h4 class="tb-table--subtitle"><strong>2018</strong></h4>
							</td>
						</thead>
						<tbody>
							<tr>
								<td>
									<h5 class="tb-table--type">
									<strong>Turista</strong>
									</h5>
								</td>
								<td>
									<p class="tb-table--descount">
									0,5%
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<h5 class="tb-table--type">
									<strong>Frecuente</strong>
									</h5>
								</td>
								<td>
									<p class="tb-table--descount">
									1,5%
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<h5 class="tb-table--type">
									<strong>Plata
									</h5></strong>
								</td>
								<td>
									<p class="tb-table--descount">
									3%
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<h5 class="tb-table--type">
									<strong>Oro
									</h5></strong>
								</td>
								<td>
									<p class="tb-table--descount">
									4%
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<hr class="hidden-xs black-hr">

		<div class="row">
			<div class="col-md-6">
			<h2>Consideraciones sobre Segmentos</h2>
			<br>
			<ul class="tb-pasenger-list">
				<li>
					<p>Toda persona que ingrese por primera vez al programa de beneficios Turclub será “Pasajero Turista”.</p>
				</li>
				<li>
					<p>El cambio de segmento se realizará cuando el pasajero presente movimientos en su cartola, es decir, haber adquirido un boleto a lo menos dentro del mes.</p>
				</li>
				<li>
					<p>El cambio de segmento se realizará de forma mensual y contemplará los últimos 6 meses móviles. </p>
				</li>
			</ul>
			</div>
			<div class="col-md-6">
				<h2>Caducidad de Puntos</h2>
				<br>
				<p>A partir de Enero 2018 se considerará que todo punto acumulado en un periodo mayor a 6 meses caducará automáticamente de manera retroactiva para todos los pasajeros que hayan realizado viajes en los últimos 12 meses.</p>
			</div>
		</div>

	</section>

<?php include 'includes/footer.php' ?>


<?php include 'includes/scripts.php'; ?>

</body>
</html>