<?php
$pageTitle = 'Bienvenidos a TurClub';
$pageDescription = 'Cuando eres parte del programa de beneficios Turclub, podrás canjear tus puntos en la compra de tu pasaje.';
 
include('includes/head.php');
?>

<body class"tb-page-index">

    <?php include 'includes/menu.php'; ?>

    <section class="page">
        <!--<input id="input" type="text" min="0" max="100" oninput="setPercentage()" class="input-porcent">-->
        <div class="bg-image bg-uno active" style="background-image: url('img/bg-1.jpg')"></div>
        <div class="bg-image bg-dos" style="background-image: url('img/bg-2.jpg')"></div>
        <div class="bg-image bg-tres" style="background-image: url('img/bg-3.jpg')"></div>
        <div class="bg-image bg-cuatro" style="background-image: url('img/bg-4.jpg')"></div>
        <div class="bg-image bg-cinco" style="background-image: url('img/bg-5.jpg')"></div>
        <div class="table-cell">
            <div class="circle">
                <img src="img/logo.png" alt="" class="logo">
                <div class="block uno">
                    <a href="#" class="porcent-click active response" data-value="0">
                        <span class="name">Bienvenido</span>
                        <div class="punto"></div>
                    </a>
                    <div class="content active">
                        <div class="line"></div>
                        <div class="info">
                            <div class="icon">
                                <img src="img/icon-1.png" alt="">
                            </div>
                            <div class="text">
                                <div class="title">BIENVENIDO</div>
                                <p>Al programa de pasajero frecuente de Turbus</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block dos">
                    <a href="#" class="porcent-click  response" data-value="20">
                        <span class="name">ACUMULA
                            <br>PUNTOS</span>
                        <div class="punto"></div>
                    </a>
                    <div class="content">
                        <div class="line"></div>
                        <div class="info">
                            <div class="icon">
                                <img src="img/icon-2.png" alt="">
                            </div>
                            <div class="text">
                                <div class="title">ACUMULA
                                    <br>PUNTOS</div>
                                <p>Y accede a beneficios
                                    <br>cada vez que viajas solo
                                    <br>con dictar tu RUT </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block tres">
                    <a href="#" class="porcent-click  response" data-value="40">
                        <span class="name">canjea por pasajes</span>
                        <div class="punto"></div>
                    </a>
                    <div class="content">
                        <div class="line"></div>
                        <div class="info">
                            <div class="icon">
                                <img src="img/icon-3.png" alt="">
                            </div>
                            <div class="text">
                                <p>Puedes comprar pasajes
                                    <br>con tus puntos Turclub o
                                    <br>complementar con dinero
                                    <br>el valor del pasaje</p>
                                <div class="title">CANJEA PASAJES</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block cuatro">
                    <a href="#" class="porcent-click  response" data-value="60">
                        <span class="name">BENEFICIOS
                            <br>ADICIONALES</span>
                        <div class="punto"></div>
                    </a>
                    <div class="content">
                        <div class="line"></div>
                        <div class="info">
                            <div class="icon">
                                <img src="img/icon-3.png" alt="">
                            </div>
                            <div class="text">
                                <p>Beneficios exclusivos
                                    <br>para nuestros socios</p>
                                <div class="title">BENEFICIOS
                                    <br>ADICIONALES
                                </div>
                                <a class="tb-circle__link" href="beneficios.php" >Conoce los beneficios</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block cinco">
                    <a href="#" class="porcent-click  response" data-value="80">
                        <span class="name">PROGRAMAS PARA ESTUDIANTES y trabajadores</span>
                        <div class="punto"></div>
                    </a>
                    <div class="content">
                        <div class="line"></div>
                        <div class="info">
                            <div class="icon">
                                <img src="img/icon-3.png" alt="">
                            </div>
                            <div class="text">
                                <p>Tenemos un programa
                                    <br>pensado en cada pasajero
                                    <br>y sus necesidades</p>
                                <div class="title">PROGRAMA</div>
                                <div class="title">
                                    <a href="programas.php">ESTUDIANTES</a>
                                </div>
                                <div class="title">
                                    <a href="programas.php">TRABAJADORES</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <svg class="svg-bar" viewBox="0 0 400 400">
                    <circle class="svg-circle-bg" cx="200" cy="200" r="150"></circle>
                    <circle class="svg-circle-bar" cx="200" cy="200" r="150"></circle>
                </svg>
            </div>

        </div>
    </section>

    
    <?php include 'includes/scripts.php'; ?>
</body>

</html>