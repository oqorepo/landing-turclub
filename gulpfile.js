const gulp = require('gulp'),
    browserSync = require('browser-sync').create(), //Recarga el navegador
    autoprefixer = require('gulp-autoprefixer'), //Autoprefixer
    cleanCSS = require('gulp-clean-css'), //Minifica CSS
    notify = require('gulp-notify'), //Notificaciones
    sass = require('gulp-sass'), //Copila SCSS
    sourcemaps = require('gulp-sourcemaps'), //SourceMaps
    rename = require('gulp-rename'), //Renombra archivos
    uglify = require('gulp-uglify'),
    gutil = require( 'gulp-util' ),
    ftp = require( 'vinyl-ftp' ); //Minifica JS

gulp.task('sass', function () {
    return gulp.src('./scss/sass/main.sass')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: true
        }))
        .on("error", notify.onError({
            sound: true,
            title: 'Error de copilación SCSS'
        }))
        .pipe(autoprefixer({
            versions: ['last 2 browsers']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css/'))
        .pipe(browserSync.stream());
});

gulp.task( 'deploy', function () {
 
    var conn = ftp.create( {
        host:     'ftp.oqodigital.net',
        user:     'oqodigital',
        password: 'KJ6DqN7Xx6.',
        parallel: 4,
        log:      gutil.log
    } );
 
    var globs = [
        'css/**',
        'img/**',
        'includes/**',
        'js/**',
        'scss/**',
        'favicon.ico',
        './*.php'
    ];
 
    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance
 
    return gulp.src( globs, { base: '.', buffer: false } )
        .pipe( conn.newerOrDifferentSize( '/clientes/turbus/turclub.oqodigital.net' ) ) // only upload newer files
        .pipe( conn.dest( '/clientes/turbus/turclub.oqodigital.net' ) );
 
} );

gulp.task('watch', function () {
    browserSync.init({
        proxy: 'http://localhost/turbus/landing-turclub/',
        files: ['./*.php', './*.css', './*.js', './*.scss', './*.jpg', './*.png'],
        notify: true
    });
    gulp.watch('./**/*.sass', ['sass']);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch('./**/*.js').on('change', browserSync.reload);
});

gulp.task('default', ['sass']);